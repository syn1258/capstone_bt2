const BASE_URL = "https://633ec03b83f50e9ba3b75ecb.mockapi.io";

export let getDs = () => {
    return axios({
        url: `${BASE_URL}/product`,
        method: "GET",
    });
};

export let createrDs = (sp) => {
    return axios({
        url: `${BASE_URL}/product`,
        method: "POST",
        data: sp,

    })
}

export let deleteDs = (item) => {
    return axios({
        url: `${BASE_URL}/product/${item}`,
        method: "DELETE",
    })
}

export let editDs = (id) => {
    return axios({
        url: `${BASE_URL}/product/${id}`,
        method: "GET",
    })
}
export let capNhatDs = (list) => {
    return axios({
        url: `${BASE_URL}/product/${list.id}`,
        method: "PUT",
        data: list,
    })
}
