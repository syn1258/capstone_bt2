let kiemTraRong = (value, idError) => {
    if (value.length == 0) {
        document.getElementById(idError).innerText = "Trường này không được để rổng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}