import { capNhatDs, createrDs, deleteDs, editDs, getDs } from "./js/services/spServices.js";
import { layThongTinTuForm, renderDsSp, showThongTinLenForm } from "./js/spController/spController.js";
import { sanPham } from "./js/spModel/spModel.js";



// Start Thêm Sinh Viên
let themSp = () => {
  let dataform = layThongTinTuForm();
  console.log('dataform: ', dataform);
  createrDs(dataform)
    .then((res) => {
      renderListSpSrv();
      console.log('res: ', res);

    })
    .catch((err) => {
      console.log('err: ', err);

    });
}
window.themSp = themSp;
// End Thêm Sinh Vien


// Start Chỉnh Sửa Sinh Viên
let editSpSrv = (item) => {
  editDs(item)
    .then((res) => {
      showThongTinLenForm(res.data);
      document.getElementById("IdSP").disabled = true;
    })

    .catch((err) => {
      console.log('err: ', err);
    });
}
window.editSpSrv = editSpSrv;

// End Chỉnh sửa Sinh Viên

// Start Xóa Sinh Viên
let delSp = (item) => {
  deleteDs(item)
    .then((res) => {
      console.log('res: ', res);
      renderListSpSrv();

    })
    .catch((err) => {
      console.log('err: ', err);
    });
}
window.delSp = delSp;
// End Xóa Sinh Viên

// Start Render Sinh Viên
let renderListSpSrv = () => {
  getDs()
    .then((res) => {

      let danhSachSp = res.data.map((item) => {
        return new sanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      renderDsSp(danhSachSp);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}
renderListSpSrv();
// End Render Sinh Viên

// Start Cập Nhật Sinh Viên
let capNhatSp = () => {
  let dssp = layThongTinTuForm();
  console.log('dssp: ', dssp);
  capNhatDs(dssp)
    .then((res) => {
      console.log('res: ', res);
      renderListSpSrv();
    })

    .catch((err) => {
      console.log('err: ', err);
    });
}
window.capNhatSp = capNhatSp;
// End Cập Nhật Sinh Viên

