import { CartItem } from "../Model/cartItemModel.js";
import { Phone } from "../Model/phoneModel.js";
import { getListDienThoai } from "../Service/dienThoaiService.js";
// import { addToCart } from "./cartController.js";
import { addToCart, renderDienThoai } from "./phoneController.js";

let productList = [];
let samsungList = [];
let iphoneList = [];

getListDienThoai()
  .then((res) => {
    console.log(res.data);
    productList = res.data.map((item) => {
      //   for (let key in item) {
      //     console.log(item[key]);
      //   }
      item.desc = item.desc.replaceAll('"', "");
      return new Phone(
        item.name,
        item.price,
        item.screen,
        item.backCamera,
        item.frontCamera,
        item.img,
        item.desc,
        item.type
      );
    });
    renderDienThoai(productList);
    productList.forEach((phone) => {
      phone.type.toLowerCase() == "samsung"
        ? samsungList.push(phone)
        : iphoneList.push(phone);
    });
    console.log(productList);
  })
  .catch((err) => {
    console.log(err);
  });

let quickView = (hinhAnhUrl, tenSp, giaSp, moTa, cameraTruoc, cameraSau) => {
  // Bat modal
  document.querySelector("#modal").classList.add("show-modal1");
  //Them thong tin cua item hien tai vao modal
  document.querySelector("#imgModal").src = hinhAnhUrl;
  document.querySelector("#phone-name").innerText = tenSp;
  document.querySelector("#phone-price").innerText = `${giaSp}$`;
  document.querySelector("#phone-desc").innerText = moTa;
  document.querySelector("#front-camera").innerText = cameraTruoc;
  document.querySelector("#back-camera").innerText = cameraSau;

  let addCartBtn = document.querySelector("#addToCart");
  addCartBtn.onclick = function () {
    let index = productList.findIndex((item) => {
      return tenSp == item.name;
    });
    if (index == -1) return;
    addToCart(index);
    let currentItem = productList[index];
    let newItem = { product: { ...currentItem }, quantity: 2 };
    let cartItem = new CartItem(newItem.product, newItem.quantity);
    // console.log(currentItem);
    // let cartItem = new CartItem(
    //   currentItem.name,
    //   currentItem.price,
    //   currentItem.screen,
    //   currentItem.frontCamera,
    //   currentItem.backCamera,
    //   2
    // );
    console.log(cartItem);
  };

  // Them event click vao nut "Add to Cart", pass ten san pham vao
};

window.quickView = quickView;
